'use strict';

const express = require('express');
const router = express.Router();
const { getUsers, createUser,
  updateUser, deleteUser } = require('../services/user.service');
const { hasId, hasUser } = require('../middlewares/id.middlewares');
/* GET users listing. */


router.get('/', (req, res, next) => {
  const result = getUsers();
  if (result)
    res.send(result);
  else
    res.status(400).send('Error in getting users');
});

router.get('/:id', (req, res, next) => {
  const result = getUsers(req.params.id);

  if (result)
    res.send(result);
  else
    res.status(400).send(`No user with such id(${req.params.id})`);
});

router.post('/', hasUser, (req, res, next) => {
  const result = createUser(req.body);

  if (result)
    res.send(`User ${JSON.stringify(req.body)} was created`);
  else
    res.status(400).send('Error in creating user');
});

router.put('/:id', hasId, (req, res, next) => {
  const result = updateUser(req.params.id, req.body);

  if (result)
    res.send(`User with ${req.params.id} id was updated`);
  else
    res.status(400).send('Error in updating user');
});

router.delete('/:id', hasId, (req, res, next) => {
  const result = deleteUser(req.params.id);

  if (result)
    res.send(`User with ${req.params.id} id was deleted`);
  else
    res.status(400).send('Error in deleting user');
});

module.exports = router;
