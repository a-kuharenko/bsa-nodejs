'use strict';

const fs = require('fs');
const hasId = (req, res, next) => {
  const id = req.params.id;
  let hasId = false;
  const users = JSON.parse(fs.readFileSync('./source/userlist.json'));
  users.forEach(user => {
    if (user._id === id)
      hasId = true;
  });
  if (hasId)
    next();
  else
    res.status(400).send(`No user with this id(${id})`);
};

const hasUser = (req, res, next) => {
  const id = req.body._id;
  let hasUser = false;
  const users = JSON.parse(fs.readFileSync('./source/userlist.json'));
  users.forEach(user => {
    if (+user._id === +id)
      hasUser = true;
  });
  if (!hasId)
    next();
  else
    res.status(400).
      send(`There is a user with this id(${id}), id must be unique`);
};



module.exports = { hasId, hasUser };
