'use strict';

const fs = require('fs');

const findUserIndexById = (id, users) => {
  let indexUser;
  users.forEach((user, index) => {
    if (+user._id === +id)
      indexUser = index;
  });
  return indexUser;
};

const saveData = data => {
  if (data) {
    const users = JSON.parse(fs.readFileSync('./source/userlist.json', 'utf8'));
    users.push(data);
    fs.writeFileSync('./source/userlist.json', JSON.stringify(users), 'utf8');
    return true;
  }
  return false;
};

const updateData = (id, data) => {
  if (id && data) {
    const users = JSON.parse(fs.readFileSync('./source/userlist.json', 'utf8'));
    users[findUserIndexById(id, users)] = data;
    fs.writeFileSync('./source/userlist.json', JSON.stringify(users), 'utf8');
    return true;
  }
  return false;
};

const deleteData = id => {
  if (id) {
    const users = JSON.parse(fs.readFileSync('./source/userlist.json', 'utf8'));
    users.splice(findUserIndexById(id, users), 1);
    fs.writeFileSync('./source/userlist.json', JSON.stringify(users), 'utf8');
    return true;
  }
  return false;
};

module.exports = { saveData, updateData, deleteData, findUserIndexById };
