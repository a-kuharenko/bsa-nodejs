#BSA-NODEJS
#This is my first server on Express
https://bsa-nodejs-ku.herokuapp.com/

#### How to start the code

1. `git clone https://a-kuharenko@bitbucket.org/a-kuharenko/bsa-nodejs.git`
2. `cd bsa-nodejs`
3. `npm i`
4. `npm start`
5. 	By default server running on [localhost:3000](http://localhost:3000)

### This server can handle the list of following requests:

* ##**GET**: `/user`  
(get an array of all users)  
###
***./roures/user.js***
````js
router.get('/', (req, res, next) => {
  const result = getUsers();
  if (result)
    res.send(result);
  else
    res.status(400).send('Error in getting users');
});
````
###
***./services/user.service.js***
````js
const getUsers = id => {
  const users = JSON.parse(fs.readFileSync('./source/userlist.json'));
  if (!id)
    return users;
  return users[findUserIndexById(id, users)];
};
````

* ##**GET:** `/user/:id`  
(get an user from array by id)
###
***./roures/user.js***
````js
router.get('/:id', (req, res, next) => {
  const result = getUsers(req.params.id);
  if (result)
    res.send(result);
  else
    res.status(400).send(`No user with such id(${req.params.id})`);
});
````
###
***./services/user.service.js***
````js
const getUsers = id => {
  const users = JSON.parse(fs.readFileSync('./source/userlist.json'));
  if (!id)
    return users;
  return users[findUserIndexById(id, users)];
};
````
* ##**POST:** `/user`  
(create user according to the data from the request body)
###
***./roures/user.js***
````js
router.post('/', hasUser, (req, res, next) => {
  const result = createUser(req.body);
  if (result)
    res.send(`User ${JSON.stringify(req.body)} was created`);
  else
    res.status(400).send('Error in creating user');
});
````
###
***./services/user.service.js***
````js
const createUser = user => {
  if (user)
    return saveData(user);
  return null;
};
````
###
***./repositories/user.repository.js***
````js
const saveData = data => {
  if (data) {
    const users = JSON.parse(fs.readFileSync('./source/userlist.json', 'utf8'));
    users.push(data);
    fs.writeFileSync('./source/userlist.json', JSON.stringify(users), 'utf8');
    return true;
  }
  return false;
};
````
###
***./middlewares/user.middlewares.js***
````js
const hasUser = (req, res, next) => {
  const id = req.body._id;
  let hasId = false;
  const users = JSON.parse(fs.readFileSync('./source/userlist.json'));
  users.forEach(user => {
    if (user._id === id)
      hasId = true;
  });
  if (!hasId)
    next();
  else
    res.status(400).
      send(`There is a user with this id(${id}), id must be unique`);
};
````
* ##**PUT:** `/user/:id`  
(update user according to the data from the request body)
###
***./roures/user.js***
````js
router.post('/', hasUser, (req, res, next) => {
  const result = createUser(req.body);
  if (result)
    res.send(`User ${JSON.stringify(req.body)} was created`);
  else
    res.status(400).send('Error in creating user');
});
````
###
***./services/user.service.js***
````js
const updateUser = (id, user) => {
  if (id && user)
    return updateData(id, user);
  return null;
};
````
###
***./repositories/user.repository.js***
````js
const updateData = (id, data) => {
  if (id && data) {
    const users = JSON.parse(fs.readFileSync('./source/userlist.json', 'utf8'));
    users[findUserIndexById(id, users)] = data;
    fs.writeFileSync('./source/userlist.json', JSON.stringify(users), 'utf8');
    return true;
  }
  return false;
};
````
###
***./middlewares/user.middlewares.js***
````js
const hasId = (req, res, next) => {
  const id = req.params.id;
  let hasId = false;
  const users = JSON.parse(fs.readFileSync('./source/userlist.json'));
  users.forEach(user => {
    if (user._id === id)
      hasId = true;
  });
  if (hasId)
    next();
  else
    res.status(400).send(`No user with this id((${id})`);
};
````
* ##**DELETE:** `/user/:id`  
(delete one user by ID)
###
***./roures/user.js***
````js
router.delete('/:id', hasId, (req, res, next) => {
  const result = deleteUser(req.params.id);
  if (result)
    res.send(`User with ${req.params.id} id was deleted`);
  else
    res.status(400).send('Error in deleting user');
});
````
###
***./services/user.service.js***
````js
const deleteUser = id => {
  if (id)
    return deleteData(id);
  return null;
};
````
###
***./repositories/user.repository.js***
````js
const deleteData = id => {
  if (id) {
    const users = JSON.parse(fs.readFileSync('./source/userlist.json', 'utf8'));
    users.splice(findUserIndexById(id, users), 1);
    fs.writeFileSync('./source/userlist.json', JSON.stringify(users), 'utf8');
    return true;
  }
  return false;
};
````